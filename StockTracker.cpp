#include "StockTracker.h"
#include <string>
#include <iostream>
#include <map>
#include <utility>


StockTracker::StockTracker(){

}
void StockTracker::registerTicker(string tickerSymbol, int sharesOutstanding){
    stocks.insert(make_pair(tickerSymbol, sharesOutstanding));
    //std::cout << "shares: " << stocks[tickerSymbol] << std::endl;
}

bool StockTracker::isTicker(string tickerSymbol){
  if (stocks.count(tickerSymbol) == 0){
    return false;
    }
  else{
    return true;
  }

}

int StockTracker::getSharesOutstanding(string tickerSymbol){
  if (!stocks.empty()){
    if (stocks.count(tickerSymbol) != 0){
      //std::cout << "Number of shares: " << stocks[tickerSymbol] << std::endl;
      return stocks[tickerSymbol];
    }
  }
  else{
    return 0;
  }
}


void StockTracker::updateCurrentPrice(string tickerSymbol, double price){
  if (stockPrices.count(tickerSymbol) == 0){
    stockPrices.insert({tickerSymbol, price});
  }
  else{
    stockPrices[tickerSymbol] = price;
    //std::cout << tickerSymbol << " price: " << price << std::endl;
    //std::cout << "Stock " << tickerSymbol << ": " << stockPrices[tickerSymbol] << std::endl;
  }
}

double StockTracker::getCurrentPrice(string tickerSymbol){
  if (stockPrices.count(tickerSymbol) == 0){
    return 0.0;
  }
  else{
    return stockPrices[tickerSymbol];
  }
}

double StockTracker::getMarketCap(string tickerSymbol){
  if (stockPrices.count(tickerSymbol) == 0){
    return 0.0;
  }
  else{
    return stockPrices[tickerSymbol] * stocks[tickerSymbol];
  }
}

vector<string> StockTracker::topMarketCaps(int k){

  vector<string> caps;
  map<string, int>::iterator it1 = stocks.begin();
  map<string, double>::iterator it2 = stockPrices.begin();
  multimap <double, string> marketCap;

  for (int i = 0; i < stocks.size(); i++){
  marketCap.insert(make_pair((((*it1).second) * ((*it2).second)), (*it1).first));
  it1++;
  it2++;
}
  multimap<double,string>::iterator it3 = marketCap.end();
  it3--;
  for (int i = 0; i < k; i++){
    caps.push_back((*it3).second);
    it3--;
  }
    return caps;
}
